import Home from '../components/home.vue'
import Architect from '../components/architect.vue'
import Ambienti from '../components/ambienti.vue'
import Portfolio from '../components/portfolio.vue'
import SinglePortfolio from '../components/single_portfolio.vue'
import Contact from '../components/contact.vue'

export const routes = [
    {path : '/' , component : Home},
    {path : '/architect' , component : Architect},
    {path : '/ambienti' , component : Ambienti},
    {path : '/portfolio' , component : Portfolio},
    {path : '/portfolio/single' , component : SinglePortfolio},
    {path : '/contact' , component : Contact},
];